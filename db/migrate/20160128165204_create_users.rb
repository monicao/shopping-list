class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, null: false
      t.string :password
    end
    # Add a unique index to the username column
    # This asks the database to validate the column
    # We will have an ActiveRecord validation as well.
    add_index :users, :username, unique: true
  end
end
