class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.boolean :completed
      t.references :user
    end
    add_index :items, :user_id
  end
end
