class User < ActiveRecord::Base
  has_many :items

  validates :username, uniqueness: true

  # NOTE: We NEVER store a user's password in the database
  # in a real production application.
  # We would store the hashed password (google: BCrypt)
  validates :password, presence: true
end